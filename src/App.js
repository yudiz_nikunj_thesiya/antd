import "antd/dist/antd.css";
import { Route, Routes } from "react-router-dom";
import Calender from "./pages/Calender";
import DataTable from "./pages/DataTable";
import Home from "./pages/Home";
import Login from "./pages/Login";
import Register from "./pages/Register";
import RegularTable from "./pages/RegularTable";

function App() {
	return (
		<div className="App">
			<Routes>
				<Route path="/" element={<Home />} />
				<Route path="/login" element={<Login />} />
				<Route path="/register" element={<Register />} />
				<Route path="/regulartable" element={<RegularTable />} />
				<Route path="/datatable" element={<DataTable />} />
				<Route path="/calender" element={<Calender />} />
			</Routes>
		</div>
	);
}

export default App;
