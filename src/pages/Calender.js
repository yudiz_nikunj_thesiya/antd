import { Layout, Calendar } from "antd";
import React, { useState } from "react";
import "../styles/nav.scss";

import SiderMenu from "../components/SiderMenu";
import Nav from "../components/Nav";
import MobileBar from "../components/MobileBar";

const { Content, Footer } = Layout;

const Calender = () => {
	const [collapsed, setCollapsed] = useState(false);
	const [mobileBar, setMobileBar] = useState(false);

	const date = new Date();

	function onPanelChange(value, mode) {
		console.log(value.format("YYYY-MM-DD"), mode);
	}

	return (
		<Layout className="home">
			<SiderMenu collapsed={collapsed} />
			{mobileBar === true && <MobileBar setMobileBar={setMobileBar} />}

			<Layout className="site-layout">
				<Nav
					collapsed={collapsed}
					setCollapsed={setCollapsed}
					mobileBar={mobileBar}
					setMobileBar={setMobileBar}
				/>

				<Content
					className="site-layout-background"
					style={{
						margin: "24px 16px",
						boxSizing: "border-box",
						display: "flex",
						flexDirection: "column",
						gap: 16,
						padding: 24,
						height: 280,
						overflowY: "scroll",
					}}
				>
					<Calendar onPanelChange={onPanelChange} />
				</Content>
				<Footer
					style={{
						textAlign: "center",
						backgroundColor: "#fff",
						color: "#001628",
					}}
				>
					REACTDASH ©{date.getFullYear()} Designed By Nikunj Thesiya
				</Footer>
			</Layout>
		</Layout>
	);
};

export default Calender;
