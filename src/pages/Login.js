import {
	Layout,
	Typography,
	Form,
	Input,
	Button,
	Checkbox,
	Space,
	Breadcrumb,
} from "antd";
import React, { useState } from "react";
import "../styles/nav.scss";
import {
	InfoCircleOutlined,
	EyeInvisibleOutlined,
	EyeTwoTone,
	HomeOutlined,
	FileImageOutlined,
} from "@ant-design/icons";
import SiderMenu from "../components/SiderMenu";
import Nav from "../components/Nav";
import MobileBar from "../components/MobileBar";

const { Content, Footer } = Layout;
const { Title } = Typography;

const Login = () => {
	const [collapsed, setCollapsed] = useState(false);
	const [mobileBar, setMobileBar] = useState(false);
	const date = new Date();

	const [form] = Form.useForm();
	const onFinish = (values) => {
		console.log(values);
	};
	const onReset = () => {
		console.log("btn clicked");
		form.resetFields();
	};

	return (
		<Layout className="home">
			<SiderMenu collapsed={collapsed} />
			{mobileBar === true && <MobileBar setMobileBar={setMobileBar} />}

			<Layout className="site-layout">
				<Nav
					collapsed={collapsed}
					setCollapsed={setCollapsed}
					mobileBar={mobileBar}
					setMobileBar={setMobileBar}
				/>

				<Content
					className="site-layout-background"
					style={{
						margin: "24px 16px",
						padding: 24,
						height: 280,
						display: "flex",
						flexDirection: "column",
						gap: 16,
						overflowY: "scroll",
					}}
				>
					<Breadcrumb>
						<Breadcrumb.Item href="/">
							<HomeOutlined />
						</Breadcrumb.Item>
						<Breadcrumb.Item>
							<FileImageOutlined />
							<span>Pages</span>
						</Breadcrumb.Item>
						<Breadcrumb.Item>Login</Breadcrumb.Item>
					</Breadcrumb>
					<Form
						form={form}
						layout="vertical"
						initialValues={{ requiredMarkValue: "optional" }}
						requiredMark="optional"
						name="control-hooks"
						onFinish={onFinish}
						style={{ width: "100%" }}
					>
						<Title level={3}>Login Form</Title>
						<Layout style={{ padding: " 32px 32px 8px" }}>
							<Form.Item
								label="Username"
								name="username"
								rules={[
									{
										required: true,
										message: "Please input your username",
									},
								]}
								tooltip={{
									title: "username should be unique",
									icon: <InfoCircleOutlined />,
								}}
							>
								<Input placeholder="Enter Your Username" />
							</Form.Item>
							<Form.Item
								label="Password"
								name="password"
								rules={[
									{
										required: true,
										message: "Please input your password",
									},
								]}
								tooltip="This is a required field"
							>
								<Input.Password
									placeholder="Enter Your Password"
									iconRender={(visible) =>
										visible ? <EyeTwoTone /> : <EyeInvisibleOutlined />
									}
								/>
							</Form.Item>
							<Form.Item name="remember" valuePropName="checked">
								<Checkbox>Remember me</Checkbox>
							</Form.Item>
							<Form.Item>
								<Space>
									<Button type="primary" htmlType="submit">
										Submit
									</Button>
									<Button htmlType="button" onClick={onReset}>
										Reset
									</Button>
								</Space>
							</Form.Item>
						</Layout>
					</Form>
				</Content>
				<Footer
					style={{
						textAlign: "center",
						backgroundColor: "#fff",
						color: "#001628",
					}}
				>
					REACTDASH ©{date.getFullYear()} Designed By Nikunj Thesiya
				</Footer>
			</Layout>
		</Layout>
	);
};

export default Login;
