import {
	Layout,
	Typography,
	Form,
	Input,
	Button,
	Space,
	Select,
	Breadcrumb,
	Upload,
} from "antd";
import ImgCrop from "antd-img-crop";
import React, { useState } from "react";
import "../styles/nav.scss";
import {
	InfoCircleOutlined,
	EyeInvisibleOutlined,
	EyeTwoTone,
	HomeOutlined,
	FileImageOutlined,
} from "@ant-design/icons";
import SiderMenu from "../components/SiderMenu";
import Nav from "../components/Nav";
import MobileBar from "../components/MobileBar";

const { Content, Footer } = Layout;
const { Title } = Typography;
const { Option } = Select;

const selectBefore = (
	<Select defaultValue="http://" className="select-before">
		<Option value="http://">http://</Option>
		<Option value="https://">https://</Option>
	</Select>
);
const selectAfter = (
	<Select defaultValue=".com" className="select-after">
		<Option value=".com">.com</Option>
		<Option value=".com">.tech</Option>
		<Option value=".jp">.jp</Option>
		<Option value=".cn">.cn</Option>
		<Option value=".org">.org</Option>
	</Select>
);

const Register = () => {
	const [collapsed, setCollapsed] = useState(false);
	const [mobileBar, setMobileBar] = useState(false);
	const [fileList, setFileList] = useState([
		{
			uid: "-1",
			name: "image.png",
			status: "done",
			url: "https://images.unsplash.com/photo-1522075469751-3a6694fb2f61?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1180&q=80",
		},
	]);

	const date = new Date();

	const [form] = Form.useForm();
	const onFinish = (values) => {
		console.log(values);
	};
	const onReset = () => {
		console.log("btn clicked");
		form.resetFields();
	};

	const onChange = ({ fileList: newFileList }) => {
		setFileList(newFileList);
	};

	const onPreview = async (file) => {
		let src = file.url;
		if (!src) {
			src = await new Promise((resolve) => {
				const reader = new FileReader();
				reader.readAsDataURL(file.originFileObj);
				reader.onload = () => resolve(reader.result);
			});
		}
		const image = new Image();
		image.src = src;
		const imgWindow = window.open(src);
		imgWindow.document.write(image.outerHTML);
	};

	return (
		<Layout className="home">
			<SiderMenu collapsed={collapsed} />
			{mobileBar === true && <MobileBar setMobileBar={setMobileBar} />}

			<Layout className="site-layout">
				<Nav
					collapsed={collapsed}
					setCollapsed={setCollapsed}
					mobileBar={mobileBar}
					setMobileBar={setMobileBar}
				/>

				<Content
					className="site-layout-background"
					style={{
						margin: "24px 16px",
						boxSizing: "border-box",
						display: "flex",
						flexDirection: "column",
						gap: 16,
						padding: 24,
						height: 280,
						overflowY: "scroll",
					}}
				>
					<Form
						form={form}
						layout="vertical"
						initialValues={{ requiredMarkValue: "optional" }}
						requiredMark="optional"
						name="control-hooks"
						onFinish={onFinish}
						style={{ width: "100%" }}
					>
						<Breadcrumb>
							<Breadcrumb.Item href="/">
								<HomeOutlined />
							</Breadcrumb.Item>
							<Breadcrumb.Item>
								<FileImageOutlined />
								<span>Pages</span>
							</Breadcrumb.Item>
							<Breadcrumb.Item>Register</Breadcrumb.Item>
						</Breadcrumb>

						<Title style={{ marginTop: 16 }} level={3}>
							Registration Form
						</Title>
						<Layout style={{ padding: " 32px 32px 8px" }}>
							<ImgCrop rotate>
								<Upload
									action="https://www.mocky.io/v2/5cc8019d300000980a055e76"
									listType="picture-card"
									fileList={fileList}
									onChange={onChange}
									onPreview={onPreview}
								>
									{fileList.length < 1 && "+ Upload"}
								</Upload>
							</ImgCrop>
							<Form.Item
								label="Email"
								name="email"
								rules={[
									{
										required: true,
										message: "Please input your email",
									},
								]}
								tooltip={{
									title: "please enter your email",
									icon: <InfoCircleOutlined />,
								}}
							>
								<Input placeholder="Enter Your Email" />
							</Form.Item>
							<Form.Item
								label="Username"
								name="username"
								rules={[
									{
										required: true,
										message: "Please input your username",
									},
								]}
								tooltip={{
									title: "username should be unique",
									icon: <InfoCircleOutlined />,
								}}
							>
								<Input placeholder="Enter Your Username" />
							</Form.Item>
							<Form.Item
								label="Personal Website"
								name="website"
								rules={[
									{
										required: true,
										message: "Please input your website link",
									},
								]}
								tooltip={{
									title: "eneter here website name",
									icon: <InfoCircleOutlined />,
								}}
							>
								<Input
									addonBefore={selectBefore}
									addonAfter={selectAfter}
									defaultValue="nikunjthesiya"
								/>
							</Form.Item>
							<Form.Item
								label="Password"
								name="password"
								rules={[
									{
										required: true,
										message: "Please input your password",
									},
								]}
								tooltip="This is a required field"
							>
								<Input.Password
									placeholder="Enter Your Password"
									iconRender={(visible) =>
										visible ? <EyeTwoTone /> : <EyeInvisibleOutlined />
									}
								/>
							</Form.Item>
							<Form.Item>
								<Space>
									<Button type="primary" htmlType="submit">
										Register
									</Button>
									<Button htmlType="button" onClick={onReset}>
										Reset
									</Button>
								</Space>
							</Form.Item>
						</Layout>
					</Form>
				</Content>
				<Footer
					style={{
						textAlign: "center",
						backgroundColor: "#fff",
						color: "#001628",
					}}
				>
					REACTDASH ©{date.getFullYear()} Designed By Nikunj Thesiya
				</Footer>
			</Layout>
		</Layout>
	);
};

export default Register;
