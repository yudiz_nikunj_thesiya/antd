import { Layout, Row, Col, Typography, Breadcrumb } from "antd";
import React, { useState } from "react";
import "../styles/nav.scss";
import {
	BookFilled,
	PieChartFilled,
	DollarCircleFilled,
	TwitterOutlined,
	HomeOutlined,
	DashboardOutlined,
} from "@ant-design/icons";
import "../styles/home.scss";
import SiderMenu from "../components/SiderMenu";
import Nav from "../components/Nav";
import CardView from "../components/CardView";
import InfoCard from "../components/InfoCard";
import MobileBar from "../components/MobileBar";

const { Content, Footer } = Layout;
const { Title } = Typography;

const Home = () => {
	const [collapsed, setCollapsed] = useState(false);
	const [mobileBar, setMobileBar] = useState(false);

	const date = new Date();

	return (
		<Layout className="home">
			<SiderMenu collapsed={collapsed} />
			{mobileBar === true && <MobileBar setMobileBar={setMobileBar} />}

			<Layout className="site-layout">
				<Nav
					collapsed={collapsed}
					setCollapsed={setCollapsed}
					mobileBar={mobileBar}
					setMobileBar={setMobileBar}
				/>

				<Content
					className="site-layout-background"
					style={{
						margin: "24px 16px",
						display: "flex",
						flexDirection: "column",
						gap: 16,
						padding: 24,
						height: 280,
						overflowY: "scroll",
					}}
				>
					<Breadcrumb>
						<Breadcrumb.Item href="/">
							<HomeOutlined />
						</Breadcrumb.Item>
						<Breadcrumb.Item href="/">
							<DashboardOutlined />
							<span>Dashboard</span>
						</Breadcrumb.Item>
					</Breadcrumb>
					<Row className="card-container" gutter={[16, 16]}>
						<Col span={6} xs={{ span: 24 }} md={{ span: 12 }} lg={{ span: 6 }}>
							<InfoCard
								heading="Bookings"
								number="184"
								icon={
									<BookFilled
										className="icon"
										style={{ fontSize: 32, color: "#FC930B" }}
									/>
								}
							/>
						</Col>
						<Col span={6} xs={{ span: 24 }} md={{ span: 12 }} lg={{ span: 6 }}>
							<InfoCard
								heading="Website Visits"
								number="75.521"
								icon={
									<PieChartFilled
										className="icon"
										style={{ fontSize: 32, color: "#DD2566" }}
									/>
								}
							/>
						</Col>
						<Col span={6} xs={{ span: 24 }} md={{ span: 12 }} lg={{ span: 6 }}>
							<InfoCard
								heading="Revenue"
								number="$34,245"
								icon={
									<DollarCircleFilled
										className="icon"
										style={{ fontSize: 32, color: "#4CA851" }}
									/>
								}
							/>
						</Col>
						<Col span={6} xs={{ span: 24 }} md={{ span: 12 }} lg={{ span: 6 }}>
							<InfoCard
								heading="Followers"
								number="+245"
								icon={
									<TwitterOutlined
										className="icon"
										style={{ fontSize: 32, color: "#20C1D5" }}
									/>
								}
							/>
						</Col>
					</Row>
					<Title style={{ marginTop: 16 }} level={3}>
						Most Liked Blogs
					</Title>
					<Row className="card-container" gutter={[24, 24]}>
						<Col
							span={6}
							xs={{ span: 24 }}
							md={{ span: 12 }}
							lg={{ span: 8 }}
							xxl={6}
						>
							<CardView
								img1="https://images.unsplash.com/photo-1568715684971-9ac138754ab9?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1765&q=80"
								img2="https://images.unsplash.com/photo-1569446477703-88dd83d59426?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1770&q=80"
								heading="Cozy 5 Stars Apartment"
								desc="The place is close to Barceloneta Beach and bus stop just 2 min by
						walk and near to 'Naviglio' where you can enjoy the main night life
						in Barcelona."
							/>
						</Col>
						<Col
							span={6}
							xs={{ span: 24 }}
							md={{ span: 12 }}
							lg={{ span: 8 }}
							xxl={6}
						>
							<CardView
								img1="https://images.pexels.com/photos/1170412/pexels-photo-1170412.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=2"
								img2="https://images.unsplash.com/photo-1542744173-8e7e53415bb0?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1770&q=80"
								heading="Office Studio"
								desc="The place is close to Metro Station and bus stop just 2 min by walk and near to 'Naviglio' where you can enjoy the night life in London, UK."
							/>
						</Col>
						<Col
							span={6}
							xs={{ span: 24 }}
							md={{ span: 12 }}
							lg={{ span: 8 }}
							xxl={6}
						>
							<CardView
								img1="https://images.pexels.com/photos/1055068/pexels-photo-1055068.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=2"
								img2="https://images.unsplash.com/photo-1616799677131-b283db711366?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1772&q=80"
								heading="Beautiful Castle"
								desc="The place is close to Metro Station and bus stop just 2 min by walk and near to 'Naviglio' where you can enjoy the main night life in Milan."
							/>
						</Col>
						<Col
							span={6}
							xs={{ span: 24 }}
							md={{ span: 12 }}
							lg={{ span: 8 }}
							xxl={6}
						>
							<CardView
								img1="https://images.pexels.com/photos/1170412/pexels-photo-1170412.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=2"
								img2="https://images.unsplash.com/photo-1542744173-8e7e53415bb0?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1770&q=80"
								heading="Office Studio"
								desc="The place is close to Metro Station and bus stop just 2 min by walk and near to 'Naviglio' where you can enjoy the night life in London, UK."
							/>
						</Col>
					</Row>
				</Content>
				<Footer
					style={{
						textAlign: "center",
						backgroundColor: "#fff",
						color: "#001628",
					}}
				>
					REACTDASH ©{date.getFullYear()} Designed By Nikunj Thesiya
				</Footer>
			</Layout>
		</Layout>
	);
};

export default Home;
