import React, { useState } from "react";
import { Breadcrumb, Layout, Table, Typography } from "antd";
import SiderMenu from "../components/SiderMenu";
import MobileBar from "../components/MobileBar";
import Nav from "../components/Nav";
import { HomeOutlined, AppstoreOutlined } from "@ant-design/icons";

const { Content, Footer } = Layout;
const { Title } = Typography;

const columns = [
	{
		title: "Name",
		dataIndex: "name",
		filters: [
			{
				text: "Joe",
				value: "Joe",
			},
			{
				text: "Category 1",
				value: "Category 1",
				children: [
					{
						text: "Yellow",
						value: "Yellow",
					},
					{
						text: "Pink",
						value: "Pink",
					},
				],
			},
			{
				text: "Category 2",
				value: "Category 2",
				children: [
					{
						text: "Green",
						value: "Green",
					},
					{
						text: "Black",
						value: "Black",
					},
				],
			},
		],
		filterMode: "tree",
		filterSearch: true,
		onFilter: (value, record) => record.name.includes(value),
		width: "30%",
	},
	{
		title: "Age",
		dataIndex: "age",
		sorter: (a, b) => a.age - b.age,
	},
	{
		title: "Address",
		dataIndex: "address",
		filters: [
			{
				text: "London",
				value: "London",
			},
			{
				text: "New York",
				value: "New York",
			},
		],
		onFilter: (value, record) => record.address.startsWith(value),
		filterSearch: true,
		width: "40%",
	},
];

const data = [
	{
		key: "1",
		name: "John Brown",
		age: 32,
		address: "New York No. 1 Lake Park",
	},
	{
		key: "2",
		name: "Jim Green",
		age: 42,
		address: "London No. 1 Lake Park",
	},
	{
		key: "3",
		name: "Joe Black",
		age: 32,
		address: "Sidney No. 1 Lake Park",
	},
	{
		key: "4",
		name: "Jim Red",
		age: 32,
		address: "London No. 2 Lake Park",
	},
];

const RegularTable = () => {
	const [collapsed, setCollapsed] = useState(false);
	const [mobileBar, setMobileBar] = useState(false);

	const date = new Date();
	return (
		<Layout className="home">
			<SiderMenu collapsed={collapsed} />
			{mobileBar === true && <MobileBar setMobileBar={setMobileBar} />}

			<Layout className="site-layout">
				<Nav
					collapsed={collapsed}
					setCollapsed={setCollapsed}
					mobileBar={mobileBar}
					setMobileBar={setMobileBar}
				/>

				<Content
					className="site-layout-background"
					style={{
						margin: "24px 16px",
						boxSizing: "border-box",
						display: "flex",
						flexDirection: "column",
						gap: 16,
						padding: 24,
						height: 280,
						overflowY: "scroll",
					}}
				>
					<Breadcrumb>
						<Breadcrumb.Item href="/">
							<HomeOutlined />
						</Breadcrumb.Item>
						<Breadcrumb.Item>
							<AppstoreOutlined />
							<span>Tables</span>
						</Breadcrumb.Item>
						<Breadcrumb.Item>Regular Tables</Breadcrumb.Item>
					</Breadcrumb>
					<Title level={3}>Regular Table</Title>
					<Table columns={columns} dataSource={data} />
				</Content>
				<Footer
					style={{
						textAlign: "center",
						backgroundColor: "#fff",
						color: "#001628",
					}}
				>
					REACTDASH ©{date.getFullYear()} Designed By Nikunj Thesiya
				</Footer>
			</Layout>
		</Layout>
	);
};

export default RegularTable;
