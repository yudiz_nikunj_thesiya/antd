import React, { useState } from "react";
import { Breadcrumb, Layout, Table, Typography } from "antd";
import SiderMenu from "../components/SiderMenu";
import MobileBar from "../components/MobileBar";
import Nav from "../components/Nav";
import {
	EditOutlined,
	DeleteOutlined,
	HomeOutlined,
	AppstoreOutlined,
} from "@ant-design/icons";

const { Content, Footer } = Layout;
const { Title } = Typography;

const columns = [
	{
		title: "Full Name",
		width: 100,
		dataIndex: "name",
		key: "name",
		fixed: "left",
	},
	{
		title: "Age",
		width: 100,
		dataIndex: "age",
		key: "age",
		fixed: "left",
	},
	{
		title: "Column 1",
		dataIndex: "address",
		key: "1",
		width: 150,
	},
	{
		title: "Column 2",
		dataIndex: "address",
		key: "2",
		width: 150,
	},
	{
		title: "Column 3",
		dataIndex: "address",
		key: "3",
		width: 150,
	},
	{
		title: "Column 4",
		dataIndex: "address",
		key: "4",
		width: 150,
	},
	{
		title: "Column 5",
		dataIndex: "address",
		key: "5",
		width: 150,
	},
	{
		title: "Column 6",
		dataIndex: "address",
		key: "6",
		width: 150,
	},
	{
		title: "Column 7",
		dataIndex: "address",
		key: "7",
		width: 150,
	},
	{ title: "Column 8", dataIndex: "address", key: "8" },
	{
		title: "Action",
		key: "operation",
		fixed: "right",
		width: 100,
		render: () => (
			<div
				style={{
					display: "flex",
					alignItems: "center",
					gap: 10,
				}}
			>
				<span
					style={{
						padding: 8,
						backgroundColor: "#bae7ff",
						color: "#003a8c",
						cursor: "pointer",
						display: "flex",
						alignItems: "center",
						justifyContent: "center,",
					}}
				>
					<EditOutlined />
				</span>
				<span
					style={{
						padding: 8,
						backgroundColor: "#ffccc7",
						color: "#cf1322",
						cursor: "pointer",
						display: "flex",
						alignItems: "center",
						justifyContent: "center,",
					}}
				>
					<DeleteOutlined />
				</span>
			</div>
		),
	},
];

const data = [];
for (let i = 0; i < 100; i++) {
	data.push({
		key: i,
		name: `Edrward ${i}`,
		age: 32,
		address: `London Park no. ${i}`,
	});
}
const DataTable = () => {
	const [collapsed, setCollapsed] = useState(false);
	const [mobileBar, setMobileBar] = useState(false);

	const date = new Date();
	return (
		<Layout className="home">
			<SiderMenu collapsed={collapsed} />
			{mobileBar === true && <MobileBar setMobileBar={setMobileBar} />}

			<Layout className="site-layout">
				<Nav
					collapsed={collapsed}
					setCollapsed={setCollapsed}
					mobileBar={mobileBar}
					setMobileBar={setMobileBar}
				/>

				<Content
					className="site-layout-background"
					style={{
						margin: "24px 16px",
						boxSizing: "border-box",
						display: "flex",
						flexDirection: "column",
						gap: 16,
						padding: 24,
						height: 280,
						overflowY: "scroll",
					}}
				>
					<Breadcrumb>
						<Breadcrumb.Item href="/">
							<HomeOutlined />
						</Breadcrumb.Item>
						<Breadcrumb.Item>
							<AppstoreOutlined />
							<span>Tables</span>
						</Breadcrumb.Item>
						<Breadcrumb.Item>Data Tables</Breadcrumb.Item>
					</Breadcrumb>
					<Title level={3}>Data Table</Title>
					<Table
						columns={columns}
						dataSource={data}
						scroll={{ x: 1500, y: 300 }}
					/>
				</Content>
				<Footer
					style={{
						textAlign: "center",

						backgroundColor: "#fff",
						color: "#001628",
					}}
				>
					REACTDASH ©{date.getFullYear()} Designed By Nikunj Thesiya
				</Footer>
			</Layout>
		</Layout>
	);
};

export default DataTable;
