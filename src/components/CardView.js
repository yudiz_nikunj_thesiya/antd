import { Avatar, Card, Carousel, Image, Typography } from "antd";
import React from "react";
import {
	EditOutlined,
	EllipsisOutlined,
	SettingOutlined,
} from "@ant-design/icons";

const { Meta } = Card;
const { Paragraph } = Typography;

const CardView = ({ img1, img2, heading, desc }) => {
	return (
		<Card
			cover={
				<Carousel autoplay="true">
					<Image
						width="100%"
						height={220}
						style={{ objectFit: "contain" }}
						alt="example"
						src={img1}
					/>
					<Image
						width="100%"
						height={220}
						style={{ objectFit: "contain" }}
						alt="example"
						src={img2}
					/>
				</Carousel>
			}
			actions={[
				<SettingOutlined key="setting" />,
				<EditOutlined key="edit" />,
				<EllipsisOutlined key="ellipsis" />,
			]}
		>
			<Meta
				avatar={
					<Avatar
						style={{ backgroundColor: "#001628" }}
						src="https://joeschmoe.io/api/v1/random"
					/>
				}
				title={heading}
				description={<Paragraph ellipsis={{ rows: 2 }}>{desc}</Paragraph>}
			/>
		</Card>
	);
};

export default CardView;
