import React from "react";
import { Menu } from "antd";
import {
	DashboardFilled,
	FileImageFilled,
	AppstoreOutlined,
	LinkOutlined,
	CloseOutlined,
	CalendarOutlined,
} from "@ant-design/icons";
import "../styles/mobilebar.scss";
import { NavLink, useLocation } from "react-router-dom";

const MobileBar = ({ setMobileBar }) => {
	const location = useLocation();
	const { SubMenu } = Menu;
	return (
		<Menu
			defaultSelectedKeys={[location.pathname]}
			defaultOpenKeys={["sub1", "sub2"]}
			className="mobile-menu"
			mode="inline"
			style={{
				minHeight: "100vh",
			}}
			theme="dark"
		>
			<div
				style={{
					display: "flex",
					alignItems: "center",
					justifyContent: "space-between",
					padding: "0px 24px",
				}}
			>
				<span className="logo">REACTDASH</span>
				<span
					style={{
						display: "flex",
						alignItems: "center",
						justifyContent: "center",
						backgroundColor: "#fff",
						color: "#001628",
						padding: "16px",
						margin: "16px 0px",
						borderRadius: 50,
						cursor: "pointer",
						fontSize: 16,
					}}
					onClick={() => setMobileBar(false)}
				>
					<CloseOutlined />
				</span>
			</div>

			<Menu.Item key="/" icon={<DashboardFilled />}>
				<NavLink to="/">Dashboard</NavLink>
			</Menu.Item>
			<SubMenu key="sub1" icon={<FileImageFilled />} title="Pages">
				<Menu.Item key="/login" className="link-container">
					<NavLink to="/login">Login</NavLink>
				</Menu.Item>
				<Menu.Item key="/register">
					<NavLink to="/register">Register</NavLink>
				</Menu.Item>
			</SubMenu>
			<SubMenu key="sub2" icon={<AppstoreOutlined />} title="Tables">
				<Menu.Item key="/regulartable">
					<NavLink to="/regulartable">Regular Tables</NavLink>
				</Menu.Item>
				<Menu.Item key="/datatable">
					<NavLink to="/datatable">Data Tables</NavLink>
				</Menu.Item>
			</SubMenu>
			<Menu.Item key="/calender" icon={<CalendarOutlined />}>
				<NavLink to="/calender">Calender</NavLink>
			</Menu.Item>
			<Menu.Item key="link" icon={<LinkOutlined />}>
				<a
					href="https://www.nikunjthesiya.tech/"
					target="_blank"
					rel="noopener noreferrer"
				>
					Nikunj Thesiya
				</a>
			</Menu.Item>
		</Menu>
	);
};

export default MobileBar;
