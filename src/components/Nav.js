import React from "react";
import { Avatar, Badge, notification, Input, Layout, Popover } from "antd";
import {
	MenuUnfoldOutlined,
	MenuFoldOutlined,
	BellFilled,
	SearchOutlined,
} from "@ant-design/icons";
import "../styles/nav.scss";

const { Header } = Layout;

const openNotification = (placement) => {
	notification.info({
		message: `Notification`,
		description:
			"This is the content of the notification. This is the content of the notification. This is the content of the notification.",
		placement,
	});
};

const Nav = ({ collapsed, setCollapsed, mobileBar, setMobileBar }) => {
	return (
		<Header className="header" style={{ padding: 0 }}>
			{React.createElement(collapsed ? MenuUnfoldOutlined : MenuFoldOutlined, {
				className: "trigger",
				onClick: () => {
					setCollapsed(!collapsed);
					setMobileBar(!mobileBar);
				},
			})}

			<div className="header-right">
				<Input
					placeholder="Search"
					size="large"
					prefix={<SearchOutlined />}
					className="input"
				/>
				<div className="header-right-profile">
					<Badge
						count={8}
						onClick={() => openNotification("topRight")}
						style={{ cursor: "pointer" }}
					>
						<BellFilled className="notification" />
					</Badge>
					<Popover
						placement="bottomRight"
						title={<span>Profile</span>}
						content={
							<div>
								<p>Edit Profile</p>
								<p>Logout</p>
							</div>
						}
						trigger="click"
					>
						<Avatar
							className="avatar"
							src="https://joeschmoe.io/api/v1/random"
						/>
					</Popover>
				</div>
			</div>
		</Header>
	);
};

export default Nav;
