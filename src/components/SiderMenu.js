import React from "react";
import { Layout, Menu } from "antd";
import {
	DashboardFilled,
	FileImageFilled,
	AppstoreOutlined,
	LinkOutlined,
	CalendarOutlined,
} from "@ant-design/icons";
import "../styles/sidemenu.scss";
import { NavLink, useLocation } from "react-router-dom";

const { Sider } = Layout;

const SiderMenu = ({ collapsed, mobileBar }) => {
	const location = useLocation();
	const { SubMenu } = Menu;
	return (
		<Sider
			className="sider"
			trigger={null}
			collapsible
			collapsed={collapsed}
			style={{
				minHeight: "100vh",
			}}
		>
			<Menu
				defaultSelectedKeys={[location.pathname]}
				defaultOpenKeys={["sub1", "sub2"]}
				mode="inline"
				theme="dark"
			>
				{collapsed === true ? (
					<h2 className="logo">R</h2>
				) : (
					<h2 className="logo">REACTDASH</h2>
				)}

				<Menu.Item key="/" icon={<DashboardFilled />}>
					<NavLink to="/">Dashboard</NavLink>
				</Menu.Item>

				<SubMenu key="sub1" icon={<FileImageFilled />} title="Pages">
					<Menu.Item key="/login" className="link-container">
						<NavLink to="/login">Login</NavLink>
					</Menu.Item>
					<Menu.Item key="/register">
						<NavLink to="/register">Register</NavLink>
					</Menu.Item>
				</SubMenu>
				<SubMenu key="sub2" icon={<AppstoreOutlined />} title="Tables">
					<Menu.Item key="/regulartable">
						<NavLink to="/regulartable">Regular Tables</NavLink>
					</Menu.Item>
					<Menu.Item key="/datatable">
						<NavLink to="/datatable">Data Tables</NavLink>
					</Menu.Item>
				</SubMenu>
				<Menu.Item key="/calender" icon={<CalendarOutlined />}>
					<NavLink to="/calender">Calender</NavLink>
				</Menu.Item>
				<Menu.Item key="link" icon={<LinkOutlined />}>
					<a
						href="https://www.nikunjthesiya.tech/"
						target="_blank"
						rel="noopener noreferrer"
					>
						Nikunj Thesiya
					</a>
				</Menu.Item>
			</Menu>
		</Sider>
	);
};

export default SiderMenu;
