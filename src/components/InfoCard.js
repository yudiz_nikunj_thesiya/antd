import React from "react";
import { Avatar, Card, Image, Typography } from "antd";

const { Meta } = Card;
const { Title } = Typography;

const InfoCard = ({ heading, number, icon }) => {
	return (
		<Card
			style={{
				display: "flex",
				alignItems: "center",
				justifyContent: "space-between",
			}}
		>
			<Meta
				avatar={icon}
				title={heading}
				description={<span style={{ fontSize: "16px" }}>{number}</span>}
			/>
		</Card>
	);
};

export default InfoCard;
